package com.edanzgroup.test;

import com.edanzgroup.test.reckon.EvaluateRecommender;
import com.edanzgroup.test.reckon.SampleRecommender;
import com.edanzgroup.test.reckon.utils.DataUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Hello world!
 */
public class App {
    static final Logger logger = LogManager.getLogger(App.class.getName());

    public static void main(String... args) {
        //logger.entry();
        logger.info("Application started");
        new SampleRecommender();
        //new EvaluateRecommender();

        // DataUtil tests
        String basedir = "/home/wubin/workspace/tmp/";

        // convert files to seq files
        DataUtils.textToSeq(basedir + "text", basedir + "seq", null);

        // vectorize seq files
        DataUtils.vectorize(basedir + "seq",
            basedir + "vec", null, null, null);

        // split vectors into training set and testing set
        DataUtils.splitVectorized(basedir + "vec",
            basedir + "train", basedir + "test", 40, null);
    }
}

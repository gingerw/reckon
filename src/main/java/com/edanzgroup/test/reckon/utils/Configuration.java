package com.edanzgroup.test.reckon.utils;

import java.io.File;

/**
 * Created by Bin on 24/08/2014.
 */
public class Configuration {
    public static File getLogConfFile() {
        File dataset = null;
        try {
            dataset = new File("." + File.separator + "conf"
                    + File.separator + "log4j2.xml");
        } catch (Exception e) {
        }

        return dataset;
    }
}

package com.edanzgroup.test.reckon.utils;

import com.google.common.base.Strings;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.util.Version;
import org.apache.mahout.classifier.naivebayes.test.TestNaiveBayesDriver;
import org.apache.mahout.classifier.naivebayes.training.TrainNaiveBayesJob;
import org.apache.mahout.text.SequenceFilesFromDirectory;
import org.apache.mahout.utils.SplitInput;
import org.apache.mahout.vectorizer.SparseVectorsFromSequenceFiles;

import java.io.File;
import java.security.InvalidParameterException;

/**
 * Created by Bin on 24/08/2014.
 */
public class DataUtils {
    static final Logger logger = LogManager.getLogger(DataUtils.class.getName());

    public static File getDatasetFile() {
        File dataset = null;
        try {
            dataset = new File("." + File.separator + "data"
                + File.separator + "recommender"  + File.separator+ "dataset.csv");
        } catch (Exception e) {
        }

        return dataset;
    }

    /**
     * Converting text files to Hadoop sequence files
     * @param sampleDir
     * @param seqDir
     * @param conf
     */
    public static void textToSeq(final String sampleDir,
                                 final String seqDir,
                                 org.apache.hadoop.conf.Configuration conf) {
        SequenceFilesFromDirectory seqJob = new SequenceFilesFromDirectory();

        if (Strings.isNullOrEmpty(sampleDir) || Strings.isNullOrEmpty(seqDir)) {
            logger.error("input or output dirs cannot be null",
                new InvalidParameterException());
        }

        if (conf == null) {
            logger.warn("Hadoop conf is null, creating new one.");

            conf = getHadoopConf();
        }

        seqJob.setConf(conf);

        String[] args = {"-i", sampleDir, "-o", seqDir};
        try {
            logger.debug("\nconverting text files from: " + sampleDir
                + "\nto Hadoop sequence files at: " + seqDir);

            seqJob.run(args);
        } catch (Exception e) {
            logger.error("Failed to convert text files to sequence file", e);
        }
    }

    /**
     * Vectorize sequence files
     * @param seqDir
     * @param vectorDir
     * @param conf
     * @param analyzer
     * @param scorer
     */
    public static void vectorize(final String seqDir,
                                 final String vectorDir,
                                 org.apache.hadoop.conf.Configuration conf,
                                 Analyzer analyzer,
                                 String scorer) {
        SparseVectorsFromSequenceFiles vJob = new SparseVectorsFromSequenceFiles();

        if (Strings.isNullOrEmpty(seqDir) || Strings.isNullOrEmpty(vectorDir)) {
            logger.error("input or output dirs cannot be null",
                    new InvalidParameterException());
        }

        if (conf == null) {
            logger.warn("Hadoop conf is null, creating new one.");

            conf = getHadoopConf();
        }

        vJob.setConf(conf);

        if (analyzer == null) {
            logger.warn("Analyser is null, using default lucene StandardAnalyzer");

            analyzer = new StandardAnalyzer(Version.LUCENE_4_9);
        }

        if (Strings.isNullOrEmpty(scorer) || (!scorer.equalsIgnoreCase("tf") && !scorer.equalsIgnoreCase("tfidf"))) {
            logger.warn("Scorer cannot be identified, using tfidf");

            scorer = "tfidf";
        }

        String[] args = {"-i", seqDir, "-o", vectorDir,
            "-lnorm", "-nv", "-wt", scorer, "-s", "2", //minSupport, default is 2
            "-a", analyzer.getClass().getCanonicalName()};

        try {
            logger.debug("\nvectorizing seqence files in: " + seqDir
                    + "\nto vectors: " + vectorDir);

            vJob.run(args);
        } catch (Exception e) {
            logger.error("Failed to vectorize sequence files", e);
        }
    }

    /**
     * Split the data into training set and testing set
     * @param vDir
     * @param trainvDir
     * @param testvDir
     * @param proportion
     * @param conf
     */
    public static void splitVectorized(final String vDir,
                                final String trainvDir,
                                final String testvDir,
                                final Integer proportion,
                                org.apache.hadoop.conf.Configuration conf) {
        if (Strings.isNullOrEmpty(vDir) || Strings.isNullOrEmpty(trainvDir)
                || Strings.isNullOrEmpty(testvDir)) {
            logger.error("input or output dirs cannot be null",
                    new InvalidParameterException());
        }

        if (proportion == null)
            logger.error("proportion of data to be testing cannot be null",
                    new InvalidParameterException());

        if (proportion < 10 || proportion > 40)
            logger.error("proportion of testing data must be a real number between 10 and 40 inclusive",
                    new InvalidParameterException());

        SplitInput splitJob = new SplitInput();

        if (conf == null) {
            logger.warn("Hadoop conf is null, creating new one.");

            conf = getHadoopConf();
        }

        splitJob.setConf(conf);

        String[] args = {"-i", vDir, "--trainingOutput", trainvDir, "--testOutput", testvDir,
            "--randomSelectionPct", proportion.toString(), "--overwrite", "--sequenceFiles", "-xm", "sequential"};

        try {
            splitJob.run(args);
        } catch (Exception e) {
            logger.error("Failed to run the splitting job", e);
        }
    }

    /**
     * Train the machine for Naive Bayes classification algorithm
     * @param trainvDir
     * @param modelDir
     * @param labelIndexDir created when vectorizing
     * @param conf
     */
    public static void trainNaiveBayes(final String trainvDir,
                        final String modelDir,
                        final String labelIndexDir,
                        Configuration conf) {
        if (Strings.isNullOrEmpty(trainvDir) || Strings.isNullOrEmpty(modelDir)
                || Strings.isNullOrEmpty(labelIndexDir)) {
            logger.error("input or output dirs cannot be null",
                    new InvalidParameterException());
        }

        if (conf == null) {
            logger.warn("Hadoop conf is null, creating new one.");

            conf = getHadoopConf();
        }

        TrainNaiveBayesJob trainJob = new TrainNaiveBayesJob();

        trainJob.setConf(conf);

        String[] args = {"-i", trainvDir, "-o", modelDir, "-li", labelIndexDir, "-el", "-ow"};

        try {
            trainJob.run(args);
        } catch (Exception e) {
            logger.error("Failed to train Naive Bayes", e);
        }
    }

    /**
     * Test the Naive Bayes model
     * @param testvDir
     * @param modelDir
     * @param labelIndexDir
     * @param testingDir
     * @param conf
     */
    public static void testNaiveBayes(final String testvDir,
                                      final String modelDir,
                                      final String labelIndexDir,
                                      final String testingDir,
                                      Configuration conf) {
        if (Strings.isNullOrEmpty(testvDir) || Strings.isNullOrEmpty(modelDir)
                || Strings.isNullOrEmpty(labelIndexDir)
                || Strings.isNullOrEmpty(testingDir)) {
            logger.error("input or output dirs cannot be null",
                    new InvalidParameterException());
        }

        if (conf == null) {
            logger.warn("Hadoop conf is null, creating new one.");

            conf = getHadoopConf();
        }

        TestNaiveBayesDriver testJob = new TestNaiveBayesDriver();

        testJob.setConf(conf);

        String[] args = {"-i", testvDir, "-m", modelDir, "-l", labelIndexDir,
            "-ow", "-o", testingDir};

        try {
            testJob.run(args);
        } catch (Exception e) {
            logger.error("Failed to test Naive Bayes", e);
        }
    }

    private static org.apache.hadoop.conf.Configuration getHadoopConf() {
        return new Configuration();
    }
}

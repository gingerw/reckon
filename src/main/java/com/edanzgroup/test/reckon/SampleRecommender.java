package com.edanzgroup.test.reckon;

import com.edanzgroup.test.reckon.utils.DataUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericItemBasedRecommender;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.recommender.UserBasedRecommender;
import org.apache.mahout.cf.taste.similarity.ItemSimilarity;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by wubin on 14-8-20.
 */
public class SampleRecommender {
    static final Logger logger = LogManager.getLogger(SampleRecommender.class.getName());

    private DataModel model;
    private final int userId = 2;
    private final int numOfItems = 3;

    public SampleRecommender() {
        try {
            model = new FileDataModel(DataUtils.getDatasetFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
        run();
    }

    private void run() {
        userCf();
        itemCf();
    }

    private void userCf() {
        try {
            UserSimilarity similarity = new PearsonCorrelationSimilarity(model);

            UserNeighborhood neighborhood = new ThresholdUserNeighborhood(0.1, similarity, model);

            UserBasedRecommender recommender = new GenericUserBasedRecommender(model, neighborhood, similarity);

            List<RecommendedItem> recommendations = recommender.recommend(userId, numOfItems);
            for (RecommendedItem recommendation : recommendations) {
                logger.info(recommendation);
            }
        } catch (TasteException e) {
            e.printStackTrace();
        }
    }

    private void itemCf() {
        try {
            ItemSimilarity similarity = new PearsonCorrelationSimilarity(model);

            Recommender recommender = new GenericItemBasedRecommender(model, similarity);

            List<RecommendedItem> recommendations = recommender.recommend(userId, numOfItems);
            for (RecommendedItem recommendation : recommendations) {
               logger.info(recommendation);
            }
        } catch (TasteException e) {
            e.printStackTrace();
        }

    }
}
